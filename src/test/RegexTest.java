package test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * Created by mOPs on 30.11.16.
 */
public class RegexTest {
    public static void main(String[] args) throws IOException {

        Pattern p = Pattern.compile("<title>(.*?) - Google\\+</title>");
        String content;
        String nextGPlusURL;

        try (BufferedReader br = new BufferedReader(new FileReader("/home/mops/JavaProjects/gzip/unzip.txt"))) {

            while ((nextGPlusURL = br.readLine()) != null) {

                content = getWebPageSource(nextGPlusURL); // read only one line
                Matcher m = p.matcher(content);

                if (m.find()) {
                    System.out.println(m.group(1));
                } else
                    System.out.println("No find matches");
            }
        }
    }

    private static String getWebPageSource(String sURL) throws IOException {
        URL url = new URL(sURL);
        URLConnection urlCon = url.openConnection();
        BufferedReader in = null;

        if (urlCon.getHeaderField("Content-Encoding") != null
                && urlCon.getHeaderField("Content-Encoding").equals("gzip")) {
            in = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    urlCon.getInputStream())));
        } else {
            in = new BufferedReader(new InputStreamReader(
                    urlCon.getInputStream()));
        }

        String inputLine;
        StringBuilder sb = new StringBuilder();

        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine);
        in.close();

        return sb.toString();
    }
}
