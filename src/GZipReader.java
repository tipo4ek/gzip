import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

/**
 * Created by mOPs on 28.11.16.
 */
public class GZipReader {

    private static final String INPUT_URL = "http://www.gstatic.com/s2/sitemaps/sitemap-00004-of-50000.gz";
    //    private static final String SAVE_DIR = "d:\\tmp\\";
    private static final String SAVE_DIR = "/home/mops/JavaProjects/gzip/";
    private static final String OUTPUT_FILE = SAVE_DIR + "unzip.txt";
//    private static final String CONTENT_FILE = SAVE_DIR + "content.txt";
    private static final int BUFFER_SIZE = 4096;
    private static String saveFilePath;

    public static void main(String[] args) throws IOException {
        downloadFile(INPUT_URL, SAVE_DIR);
        gunzipIt();
        String content = getContent();
        String gPlusName = getGooglPlusName(content);
    }

    private static String getContent() throws IOException {
        //get first link
        String content;

        try (BufferedReader br = new BufferedReader(new FileReader(OUTPUT_FILE))){
            content = getWebPageSource(br.readLine()); // read only one line
            System.out.println("Content obtained");
        }
        return content;
    }

    private static String getGooglPlusName(String url) throws IOException {
        String content = getWebPageSource(url);
        Pattern p = Pattern.compile("<title>(.*?) - Google\\+</title>");
        Matcher m = p.matcher(content);

        if (m.find()) {
            return (m.group(1));
        }
        else
            return ("No find matches");
    }

    private static String getWebPageSource(String sURL) throws IOException {
        URL url = new URL(sURL);
        URLConnection urlCon = url.openConnection();
        BufferedReader in = null;

        if (urlCon.getHeaderField("Content-Encoding") != null
                && urlCon.getHeaderField("Content-Encoding").equals("gzip")) {
            in = new BufferedReader(new InputStreamReader(new GZIPInputStream(
                    urlCon.getInputStream())));
        } else {
            in = new BufferedReader(new InputStreamReader(
                    urlCon.getInputStream()));
        }

        String inputLine;
        StringBuilder sb = new StringBuilder();

        while ((inputLine = in.readLine()) != null)
            sb.append(inputLine);
        in.close();

        return sb.toString();
    }

    private static void downloadFile(String fileURL, String saveDir)
            throws IOException {
        URL url = new URL(fileURL);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        int responseCode = httpConn.getResponseCode();

        // always check HTTP response code first
        if (responseCode == HttpURLConnection.HTTP_OK) {
            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");

            if (disposition != null) {
                // extracts file name from header field
                int index = disposition.indexOf("filename=");
                if (index > 0) {
                    fileName = disposition.substring(index + 10,
                            disposition.length() - 1);
                }
            } else {
                // extracts file name from URL
                fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                        fileURL.length());
            }

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();
            saveFilePath = saveDir + File.separator + fileName;

            // opens an output stream to save into file
            FileOutputStream outputStream = new FileOutputStream(saveFilePath);

            int bytesRead = -1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            outputStream.close();
            inputStream.close();

            System.out.println("File downloaded");
        } else {
            System.out.println("No file to download. Server replied HTTP code: " + responseCode);
        }
        httpConn.disconnect();
    }

    private static void gunzipIt() {

        byte[] buffer = new byte[BUFFER_SIZE];

        try {

            GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(saveFilePath));
            FileOutputStream out = new FileOutputStream(OUTPUT_FILE);

            int len;
            while ((len = gzis.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }

            gzis.close();
            out.close();

            System.out.println("Done");
            System.out.println("See file: " + OUTPUT_FILE);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
